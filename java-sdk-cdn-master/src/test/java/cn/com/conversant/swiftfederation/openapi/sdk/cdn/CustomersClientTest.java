/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.CustomersClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.CustomerVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop

 */
public class CustomersClientTest {
    private CustomersClient customersClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        customersClient = cdnSdkClient.getCustomersClient();
    }


    @Test
    public void testListCustomers() {
        List<CustomerVO> customerVOList = customersClient.listCustomers();

        Assert.assertNotNull(customerVOList);
    }

    @Test
    public void testListChildCustomers() {
        List<CustomerVO> customerVOList = customersClient.listChildCustomers(2);

        Assert.assertNotNull(customerVOList);
    }

    @Test
    public void testGetCustomer() {
        CustomerVO customerVO = customersClient.getCustomer(1);

        Assert.assertNotNull(customerVO);
    }

    @Test
    public void testCreteCustomer() {
        CustomerVO customerVO = new CustomerVO();
        customerVO.setName("test-customer1");
        customerVO.setParentId(1);
        customerVO.setType((short) 2);

        CustomerVO customerVOResponse = customersClient.createCustomer(customerVO);

        Assert.assertNotNull(customerVOResponse);
    }

    @Test
    public void testDeleteCustomer() {
        customersClient.deleteCustomer(30097);
    }
}
