/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.LogFilesClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.AnalyticsRequestVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.LogFilesVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop
 */
public class LogFilesClientTest {
    private LogFilesClient logFilesClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        logFilesClient = cdnSdkClient.getLogFilesClient();
    }


    @Test
    public void testLogFiles() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");

        List<LogFilesVO> logFilesVOList = logFilesClient.getLogFiles(analyticsRequestVO);

        Assert.assertNotNull(logFilesVOList);
    }
}
