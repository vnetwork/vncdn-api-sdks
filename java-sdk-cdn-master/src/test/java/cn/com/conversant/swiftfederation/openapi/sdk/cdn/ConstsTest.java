/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

/**
 * @author robin-laptop
 */
public interface ConstsTest {
    String ACCESS_KEY_ID = "your access key id (Under Settings -> Access Keys)";

    String ACCESS_KEY_SECRET = "your key secret (Under Settings -> Access Keys)";
}
