/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.AnalyticsClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.TrafficUsageClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop
 */
public class TrafficUsageClientTest {
    private TrafficUsageClient trafficUsageClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        trafficUsageClient = cdnSdkClient.getTrafficUsageClient();
    }


    @Test
    public void testGetBandwidthUsage() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-12-24T07:10:00Z");
        analyticsRequestVO.setEndTime("2018-12-25T07:15:00Z");
        analyticsRequestVO.setFillFixedTime(true);

        List<BandwidthVO> bandwidthVOList = trafficUsageClient.getBandwidthUsage(analyticsRequestVO);

        Assert.assertNotNull(bandwidthVOList);
    }

    @Test
    public void testGetVolumeUsage() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-12-15T07:15:00Z");
        analyticsRequestVO.setEndTime("2018-12-16T07:15:00Z");
        analyticsRequestVO.setFillFixedTime(true);
        analyticsRequestVO.setInterval("minute");

        List<VolumeVO> volumeVOList = trafficUsageClient.getVolumeUsage(analyticsRequestVO);

        System.out.println(volumeVOList);
        Assert.assertNotNull(volumeVOList);
    }

    @Test
    public void testGetRequestNumberUsage() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-12-24T07:10:00Z");
        analyticsRequestVO.setEndTime("2018-12-25T07:15:00Z");

        List<RequestNumberVO> requestNumberVOList = trafficUsageClient.getRequestNumberUsage(analyticsRequestVO);

        Assert.assertNotNull(requestNumberVOList);
    }
}
