/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.PrefetchClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.PrefetchVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop
 */
public class PrefetchClientTest {
    private PrefetchClient prefetchClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        prefetchClient = cdnSdkClient.getPrefetchClient();
    }


    @Test
    public void testListPrefetches() {
        List<PrefetchVO> prefetchVOList = prefetchClient.listPrefetches(225446);

        Assert.assertNotNull(prefetchVOList);
    }

    @Test
    public void testListPrefetchesWithPage() {
        List<PrefetchVO> prefetchVOList = prefetchClient.listPrefetches(225446, 1);

        Assert.assertNotNull(prefetchVOList);
    }


    @Test
    public void testCreatePrefetches() {
        String[] urls = {"https://cdn.example1.com/resources/load.js?v=1.2.0",
                "https://cdn.example1.com/resources/load2.js?v=1.2.0",
                "https://cdn.example1.com/resources/load3.js?v=1.2.0"};
        prefetchClient.createPrefetches(225446, urls);
    }
}
