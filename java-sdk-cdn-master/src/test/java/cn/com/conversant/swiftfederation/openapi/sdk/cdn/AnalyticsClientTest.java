/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.AnalyticsClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop
 */
public class AnalyticsClientTest {
    private AnalyticsClient analyticsClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        analyticsClient = cdnSdkClient.getAnalyticsClient();
    }


    @Test
    public void testGetBandwidth() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");
        analyticsRequestVO.setFillFixedTime(true);
        analyticsRequestVO.setInterval("minute");

        List<BandwidthVO> bandwidthVOList = analyticsClient.getBandwidth(analyticsRequestVO);

        Assert.assertNotNull(bandwidthVOList);
    }

    @Test
    public void testGetCustomerBandwidth() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setStartTime("2018-10-29T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");
        analyticsRequestVO.setFillFixedTime(false);
        analyticsRequestVO.setInterval("day");

        List<TimestampAndValue> bandwidthVOList = analyticsClient.getCustomerBandwidth(30017, analyticsRequestVO);

        Assert.assertNotNull(bandwidthVOList);
    }

    @Test
    public void testGetOriginBandwidth() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");
        analyticsRequestVO.setFillFixedTime(true);
        analyticsRequestVO.setInterval("minute");

        List<BandwidthVO> bandwidthVOList = analyticsClient.getOriginBandwidth(analyticsRequestVO);

        Assert.assertNotNull(bandwidthVOList);
    }

    @Test
    public void testGetVolume() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-12-25T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-12-26T00:00:00Z");
        analyticsRequestVO.setFillFixedTime(true);
        analyticsRequestVO.setInterval("minute");

        List<VolumeVO> volumeVOList = analyticsClient.getVolume(analyticsRequestVO);

        System.out.println(volumeVOList);
        Assert.assertNotNull(volumeVOList);
    }

    @Test
    public void testGetCustomerVolume() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setStartTime("2018-11-25T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-12-26T00:00:00Z");
        analyticsRequestVO.setFillFixedTime(false);

        List<TimestampAndValue> volumeVOList = analyticsClient.getCustomerVolume(30017, analyticsRequestVO);

        System.out.println(volumeVOList);
        Assert.assertNotNull(volumeVOList);
    }

    @Test
    public void testGetHitVolume() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");

        List<HitVolumeVO> hitVolumeVOList = analyticsClient.getHitVolume(analyticsRequestVO);

        Assert.assertNotNull(hitVolumeVOList);
    }

    @Test
    public void testGetRequestNumber() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");

        List<RequestNumberVO> requestNumberVOList = analyticsClient.getRequestNumber(analyticsRequestVO);

        Assert.assertNotNull(requestNumberVOList);
    }

    @Test
    public void testGetHitRequestNumber() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");

        List<HitRequestNumberVO> hitRequestNumberVOList = analyticsClient.getHitRequestNumber(analyticsRequestVO);

        Assert.assertNotNull(hitRequestNumberVOList);
    }

    @Test
    public void testGetOriginRequestNumber() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");

        List<OriginRequestNumberVO> originRequestNumberVOList = analyticsClient.getOriginRequestNumber(analyticsRequestVO);

        Assert.assertNotNull(originRequestNumberVOList);
    }

    @Test
    public void testGetRequestHitRate() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");

        List<RequestHitRateVO> requestHitRateVOList = analyticsClient.getRequestHitRate(analyticsRequestVO);

        Assert.assertNotNull(requestHitRateVOList);
    }

    @Test
    public void testGetHttpCodesNumber() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-11-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-11-28T00:00:00Z");

        List<HttpCodesNumberVO> requestHitRateVOList = analyticsClient.getHttpCodesNumber(analyticsRequestVO);

        Assert.assertNotNull(requestHitRateVOList);
    }

    @Test
    public void testGetOriginHttpCodesNumber() {
        AnalyticsRequestVO analyticsRequestVO = new AnalyticsRequestVO();
        analyticsRequestVO.setDomains(new String[]{"report.conversant.com.cn"});
        analyticsRequestVO.setStartTime("2018-12-27T00:00:00Z");
        analyticsRequestVO.setEndTime("2018-12-28T00:00:00Z");

        List<HttpCodesNumberVO> requestHitRateVOList = analyticsClient.getOriginHttpCodesNumber(analyticsRequestVO);

        System.out.println(requestHitRateVOList);
        Assert.assertNotNull(requestHitRateVOList);
    }

}
