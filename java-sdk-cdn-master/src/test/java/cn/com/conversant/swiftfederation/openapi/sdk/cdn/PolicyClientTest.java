/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.PolicyClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.PurgeClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop
 */
public class PolicyClientTest {
    private PolicyClient policyClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        policyClient = cdnSdkClient.getPolicyClient();
    }

    @Test
    public void testListAccessControls() {
        List<AccessControlVO> accessControlVOList = policyClient.listAccessControls(225446);

        System.out.println(accessControlVOList);
        Assert.assertNotNull(accessControlVOList);
    }

    @Test
    public void testCreateAccessControl() {
        AccessControlVO accessControlVO = new AccessControlVO();
        accessControlVO.setName("test-access-control-5");
        accessControlVO.setMatchType(PolicyBaseVO.MatchType.PREFIX);
        accessControlVO.setUrl("/");
        accessControlVO.setType(AccessControlVO.AccessControlType.DENY);
        accessControlVO.setSubnet("192.168.1.0/24");
        accessControlVO.setLocation("CN");


        AccessControlVO newAccessControlVO = policyClient.createAccessControl(225446, accessControlVO);

        System.out.println(newAccessControlVO);
        Assert.assertNotNull(newAccessControlVO);
    }

    @Test
    public void testEditAccessControl() {
        AccessControlVO accessControlVO = new AccessControlVO();
        accessControlVO.setName("test-access-control-1");
        accessControlVO.setMatchType(PolicyBaseVO.MatchType.PREFIX);
        accessControlVO.setUrl("/edit_path");
        accessControlVO.setType(AccessControlVO.AccessControlType.ALLOW);
        accessControlVO.setSubnet("192.168.2.0/24");
        accessControlVO.setLocation("SG");


        policyClient.editAccessControl(225446,257142, accessControlVO);
    }

    @Test
    public void testDeleteAccessControl() {
        policyClient.deleteAccessControl(225446,257142);
    }

    @Test
    public void testListCacheControls() {
        List<CacheControlVO> cacheControlVOList = policyClient.listCacheControls(15652);

        Assert.assertNotNull(cacheControlVOList);
    }

    @Test
    public void testCreateCacheControl() {
        CacheControlVO cacheControlVO = new CacheControlVO();
        cacheControlVO.setName("test-cache-control-3");
        cacheControlVO.setMatchType(PolicyBaseVO.MatchType.PREFIX);
        cacheControlVO.setUrl("/");
        cacheControlVO.setHostHeader("conversant.com.cn");
        cacheControlVO.setTtl("1000");
        cacheControlVO.setIgnoreClientNoCache(true);
        cacheControlVO.setIgnoreOriginNoCache(true);
        cacheControlVO.setIgnoreQueryString(true);

        CacheControlVO newCacheControlVO = policyClient.createCacheControl(225446, cacheControlVO);
        Assert.assertNotNull(newCacheControlVO);
    }

    @Test
    public void testEditCacheControl() {
        CacheControlVO cacheControlVO = new CacheControlVO();
        cacheControlVO.setName("customer_002_test_policy");
        cacheControlVO.setMatchType(PolicyBaseVO.MatchType.PREFIX);
        cacheControlVO.setUrl("/test_edit");
        cacheControlVO.setHostHeader("conversant.com.cn");
        cacheControlVO.setTtl("86400");
        cacheControlVO.setIgnoreClientNoCache(false);
        cacheControlVO.setIgnoreOriginNoCache(false);
        cacheControlVO.setIgnoreQueryString(false);

        policyClient.editCacheControl(15652,32290, cacheControlVO);
    }

    @Test
    public void testDeleteCacheControl() {
        policyClient.deleteCacheControl(225446,257143);
    }

    @Test
    public void testListRedirections() {
        List<RedirectionVO> redirectionVOList = policyClient.listRedirections(225446);

        Assert.assertNotNull(redirectionVOList);
    }

    @Test
    public void testCreateRedirection() {
        RedirectionVO redirectionVO = new RedirectionVO();
        redirectionVO.setName("test-redirection-4");
        redirectionVO.setMatchType(PolicyBaseVO.MatchType.PREFIX);
        redirectionVO.setUrl("/");
        redirectionVO.setStatusCode(301);
        redirectionVO.setRedirectionURL("https://www.example1.com/error.html");

        RedirectionVO newRedirectionVO = policyClient.createRedirection(225446, redirectionVO);
        Assert.assertNotNull(newRedirectionVO);
    }

    @Test
    public void testEditRedirection() {
        RedirectionVO redirectionVO = new RedirectionVO();
        redirectionVO.setName("test-redirection-3");
        redirectionVO.setMatchType(PolicyBaseVO.MatchType.PREFIX);
        redirectionVO.setUrl("/edit_path");
        redirectionVO.setStatusCode(302);
        redirectionVO.setRedirectionURL("https://www.example1.com/error.html");

        policyClient.editRedirection(225446,257144, redirectionVO);
    }

    @Test
    public void testDeleteRedirection() {
        policyClient.deleteRedirection(225446,257144);
    }
}
