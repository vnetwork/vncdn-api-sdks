/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.ServicesClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.DomainVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.FileDownloadRequestVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.FileDownloadVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop

 */
public class ServicesClientTest {
    private ServicesClient servicesClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        servicesClient = cdnSdkClient.getServicesClient();
    }


    @Test
    public void testListDomains() {
        List<DomainVO> domainVOList = servicesClient.listDomains(30017L);

        System.out.println(domainVOList);
        Assert.assertNotNull(domainVOList);
    }

    @Test
    public void testGetDomain() {
        DomainVO domainVO = servicesClient.getDomain(225655);

        System.out.println(domainVO);
        Assert.assertNotNull(domainVO);
    }

    @Test
    public void testCreateDomain() {
        DomainVO domainVO = new DomainVO();
        domainVO.setName("test-cdn10.conversant.com.cn");
        domainVO.setCustomerId(30017L);
        domainVO.setOriginUrl("origin-cdn.conversant.com.cn");

        DomainVO newDomainVO = servicesClient.createDomain(domainVO);

        System.out.println(newDomainVO);
        Assert.assertNotNull(newDomainVO);

        DomainVO domainVO2 = servicesClient.getDomain(newDomainVO.getId());

        System.out.println(domainVO2);
        Assert.assertNotNull(domainVO2);
    }

    @Test
    public void testEditDomain() {
        DomainVO domainVO = new DomainVO();
        domainVO.setOriginUrl("origin-cdn2.conversant.com.cn");

        servicesClient.editDomain(225625,domainVO);
    }

    @Test
    public void testDeleteDomain() {
        servicesClient.deleteDomain(225625);
    }

    @Test
    public void testCreateFileDownload() {
        FileDownloadRequestVO fileDownloadRequestVO = new FileDownloadRequestVO();
        fileDownloadRequestVO.setName("test-lfd143");
        fileDownloadRequestVO.setCustomerId(30017L);
        fileDownloadRequestVO.setFtpPassword("abc123");

        FileDownloadVO fileDownloadVO = servicesClient.createFileDownload(fileDownloadRequestVO);

        System.out.println(fileDownloadVO);
        Assert.assertNotNull(fileDownloadVO);

        FileDownloadVO fileDownloadVO2 = servicesClient.getFileDownloads(fileDownloadVO.getId());

        System.out.println(fileDownloadVO2);
        Assert.assertNotNull(fileDownloadVO2);
    }

    @Test
    public void testListFileDownloads() {
        List<FileDownloadVO> fileDownloadVOList = servicesClient.listFileDownloads(30017L);

        System.out.println(fileDownloadVOList);
        Assert.assertNotNull(fileDownloadVOList);
    }

    @Test
    public void testGetFileDownload() {
        FileDownloadVO fileDownloadVO = servicesClient.getFileDownloads(225676);

        System.out.println(fileDownloadVO);
        Assert.assertNotNull(fileDownloadVO);
    }

    @Test
    public void testEditFileDownload() {
        FileDownloadRequestVO fileDownloadRequestVO = new FileDownloadRequestVO();
        fileDownloadRequestVO.setActive(false);
        fileDownloadRequestVO.setStreamingService(false);

        servicesClient.editFileDownload(225676, fileDownloadRequestVO);
    }

    @Test
    public void testChangeFileDownloadPassword() {
        FileDownloadRequestVO fileDownloadRequestVO = new FileDownloadRequestVO();
        fileDownloadRequestVO.setFtpPassword("abc123456");

        servicesClient.changeFileDownloadPassword(225676, fileDownloadRequestVO);
    }

    @Test
    public void testDeleteFileDownload() {
        servicesClient.deleteFileDownload(225676);
    }
}
