/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.PurgeClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.PurgeVO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

/**
 * @author robin-laptop
 */
public class PurgeClientTest {
    private PurgeClient purgeClient;

    @Before
    public void init() {
        CdnSdkClient cdnSdkClient = CdnSdkClient.build(ConstsTest.ACCESS_KEY_ID, ConstsTest.ACCESS_KEY_SECRET);
        purgeClient = cdnSdkClient.getPurgeClient();
    }


    @Test
    public void testListPurges() {
        List<PurgeVO> purgeVOList = purgeClient.listPurges(225675);

        System.out.println(purgeVOList);
        Assert.assertNotNull(purgeVOList);
    }

    @Test
    public void testCreatePurge() {
        PurgeVO purgeVO = new PurgeVO();
        purgeVO.setUrlPath("/resources/js/test1.js");
        purgeVO.setExpireTimestamp(1543306334);

        purgeClient.createPurge(225675, purgeVO);
    }
}
