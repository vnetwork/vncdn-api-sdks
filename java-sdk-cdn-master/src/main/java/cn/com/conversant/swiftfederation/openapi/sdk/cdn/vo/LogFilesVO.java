/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop

 */
public class LogFilesVO implements Serializable {
    private String domain;

    private List<LogFileSpotVO> logs;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<LogFileSpotVO> getLogs() {
        return logs;
    }

    public void setLogs(List<LogFileSpotVO> logs) {
        this.logs = logs;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LogFileRecordVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", logs=").append(logs);
        sb.append('}');
        return sb.toString();
    }

    public static class LogFileSpotVO {
        private String timestamp;

        private List<LogFileVO> values;


        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public List<LogFileVO> getValues() {
            return values;
        }

        public void setValues(List<LogFileVO> values) {
            this.values = values;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("LogFileSpotVO{");
            sb.append("timestamp='").append(timestamp).append('\'');
            sb.append(", values=").append(values);
            sb.append('}');
            return sb.toString();
        }
    }

    public static class LogFileVO {
        private String name;

        private String url;

        private long size;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public long getSize() {
            return size;
        }

        public void setSize(long size) {
            this.size = size;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("LogFileVO{");
            sb.append("name='").append(name).append('\'');
            sb.append(", url='").append(url).append('\'');
            sb.append(", size=").append(size);
            sb.append('}');
            return sb.toString();
        }
    }
}
