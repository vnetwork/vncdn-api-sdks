/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.DomainVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.FileDownloadRequestVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.FileDownloadVO;
import com.alibaba.fastjson.TypeReference;

import java.util.Collections;
import java.util.List;

/**
 * @author robin-laptop
 */
public class ServicesClient {
    private static RestClient restClient;

    public ServicesClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.CDN_API_URL, accessKeyId, accessKeySecret);
    }

    public List<DomainVO> listDomains(long customerId) {
        String uri = String.format("/v1.0/customers/%d/domains", customerId);

        List<DomainVO> domainVOList = restClient.get(uri, new TypeReference<List<DomainVO>>() {
        }.getType());

        if (domainVOList == null) {
            return Collections.emptyList();
        }

        return domainVOList;
    }

    public DomainVO getDomain(long serviceId) {
        String uri = String.format("/v1.0/domains/%d", serviceId);

        return restClient.get(uri, new TypeReference<DomainVO>() {
        }.getType());
    }

    public DomainVO createDomain(DomainVO domainVO) {
        String uri = "/v1.0/domains/";

        return restClient.post(uri, domainVO, new TypeReference<DomainVO>() {
        }.getType());
    }

    public void editDomain(long serviceId, DomainVO domainVO) {
        String uri = String.format("/v1.0/domains/%d", serviceId);

        restClient.put(uri, domainVO);
    }

    public void deleteDomain(long serviceId) {
        String uri = String.format("/v1.0/domains/%d", serviceId);

        restClient.delete(uri);
    }

    public List<FileDownloadVO> listFileDownloads(long customerId) {
        String uri = String.format("/v1.0/customers/%d/filedownloads", customerId);

        List<FileDownloadVO> fileDownloadVOList = restClient.get(uri, new TypeReference<List<FileDownloadVO>>() {
        }.getType());

        if (fileDownloadVOList == null) {
            return Collections.emptyList();
        }

        return fileDownloadVOList;
    }

    public FileDownloadVO getFileDownloads(long serviceId) {
        String uri = String.format("/v1.0/filedownloads/%d", serviceId);

        return restClient.get(uri, new TypeReference<FileDownloadVO>() {
        }.getType());
    }

    public FileDownloadVO createFileDownload(FileDownloadRequestVO fileDownloadRequestVO) {
        String uri = "/v1.0/filedownloads/";

        return restClient.post(uri, fileDownloadRequestVO, new TypeReference<FileDownloadVO>() {
        }.getType());
    }

    public void editFileDownload(long serviceId, FileDownloadRequestVO fileDownloadRequestVO) {
        String uri = String.format("/v1.1/filedownloads/%d", serviceId);

        restClient.put(uri, fileDownloadRequestVO);
    }

    public void changeFileDownloadPassword(long serviceId, FileDownloadRequestVO fileDownloadRequestVO) {
        String uri = String.format("/v1.0/filedownloads/%d/change_password", serviceId);

        restClient.put(uri, fileDownloadRequestVO);
    }

    public void deleteFileDownload(long serviceId) {
        String uri = String.format("/v1.0/filedownloads/%d", serviceId);

        restClient.delete(uri);
    }
}
