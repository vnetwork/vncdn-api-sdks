/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.utils.IOUtils;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.utils.SignatureUtils;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.ErrorResponse;
import com.alibaba.fastjson.JSON;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author robin-laptop
 */
public class RestClient {
    private static final String APPLICATION_JSON_UTF8_VALUE_STR = "application/json; charset=utf-8";

    public static final MediaType APPLICATION_JSON_UTF8_VALUE
            = MediaType.parse("application/json; charset=utf-8");

    private String url;

    private String accessKeyId;

    private String accessKeySecret;

    public RestClient(String url, String accessKeyId, String accessKeySecret) {
        this.url = url;
        this.accessKeyId = accessKeyId;
        this.accessKeySecret = accessKeySecret;
    }

    private static OkHttpClient client = new OkHttpClient.Builder().connectTimeout(180, TimeUnit.SECONDS).readTimeout(180, TimeUnit.SECONDS).build();

    public <T> T get(String uri, Type type) {
        try {
            Response response = executeGet(uri);
            return handleResponse(response, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T get(String uri, LinkedHashMap<String, String> params, Type type) {
        try {
            Response response = executeGet(uri, params);
            return handleResponse(response, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T post(String uri, Object object, Type type) {
        try {
            Response response = executePost(uri, object);
            return handleResponse(response, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void post(String uri, Object object) {
        try {
            Response response = executePost(uri, object);

            checkSuccessfulResponse(response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T put(String uri, Object object, Type type) {
        try {
            Response response = executePut(uri, object);
            return handleResponse(response, type);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void put(String uri, Object object) {
        try {
            Response response = executePut(uri, object);

            checkSuccessfulResponse(response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(String uri) {
        try {
            Response response = executeDelete(uri, null);

            checkSuccessfulResponse(response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void checkSuccessfulResponse(Response response) {
        if (!response.isSuccessful()) {
            if (response.body() == null || response.body().byteStream() == null) {
                return;
            }

            String responseBody = IOUtils.toString(response.body().byteStream());
            try {
                ErrorResponse errorResponse = deSerialize(responseBody, ErrorResponse.class);
                throw new RestClientException(errorResponse);
            } catch (Exception e) {
                throw new RestClientException(responseBody, e);
            }
        }
    }

    private <T> T handleResponse(Response response, Type type) throws IOException {
        if (response.isSuccessful()) {
            if (response.body() == null || response.body().byteStream() == null) {
                return null;
            }

            return deSerialize(response.body().byteStream(), type);
        } else {
            if (response.body() == null || response.body().byteStream() == null) {
                return null;
            }

            String responseBody = IOUtils.toString(response.body().byteStream());
            try {
                ErrorResponse errorResponse = deSerialize(responseBody, ErrorResponse.class);
                throw new RestClientException(errorResponse);
            } catch (Exception e) {
                throw new RestClientException(responseBody, e);
            }
        }
    }

    private Response executeGet(String uri) throws IOException {
        String route = url + uri;
        Request.Builder builder = new Request.Builder()
                .url(route)
                .get();

        addHeaders(builder, "GET", uri, "");
        Request request = builder.build();

        return client.newCall(request).execute();
    }

    private Response executeGet(String uri, LinkedHashMap<String, String> params) throws IOException {
        if (params == null || params.size() == 0) {
            return executeGet(uri);
        }

        String queryString = toQueryString(params);
        String route = url + uri + "?" + queryString;
        Request.Builder builder = new Request.Builder()
                .url(route)
                .get();

        addHeaders(builder, "GET", uri, queryString);
        Request request = builder.build();

        return client.newCall(request).execute();
    }

    private Response executePost(String uri, Object object) throws IOException {
        String route = url + uri;
        String requestBody = serialize(object);
        RequestBody body = RequestBody.create(APPLICATION_JSON_UTF8_VALUE, requestBody);
        Request.Builder builder = new Request.Builder()
                .url(route)
                .post(body);

        addHeaders(builder, "POST", uri, requestBody);
        Request request = builder.build();

        return client.newCall(request).execute();
    }

    private Response executePut(String uri, Object object) throws IOException {
        String route = url + uri;

        String requestBody = serialize(object);
        RequestBody body = RequestBody.create(APPLICATION_JSON_UTF8_VALUE, requestBody);
        Request.Builder builder = new Request.Builder()
                .url(route)
                .put(body);

        addHeaders(builder, "PUT", uri, requestBody);
        Request request = builder.build();

        return client.newCall(request).execute();
    }

    private Response executeDelete(String uri, Object object) throws IOException {
        String route = url + uri;

        String requestBody = serialize(object);
        RequestBody body = RequestBody.create(APPLICATION_JSON_UTF8_VALUE, requestBody);
        Request.Builder builder = new Request.Builder()
                .url(route)
                .delete(body);

        addHeaders(builder, "DELETE", uri, requestBody);
        Request request = builder.build();

        return client.newCall(request).execute();
    }

    private void addHeaders(Request.Builder builder, String requestMethod, String uri, String requestBody) {
        int nonce = SignatureUtils.getNonce();
        long now = Calendar.getInstance().getTimeInMillis();
        long utcNow = now - TimeZone.getDefault().getRawOffset();
        Date date = new Date(utcNow);
        String timestamp = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'").format(date);

        String signingString = StringUtils.upperCase(requestMethod) + "\n" +
                uri + "\n" +
                timestamp + "\n" +
                nonce + "\n" +
                accessKeyId + "\n" +
                requestBody;

        String signature = SignatureUtils.hexHmacSha256(accessKeySecret, signingString);
        String authorizationValue = "HMAC-SHA256 " + accessKeyId + ":"
                + signature;

        builder.addHeader("Authorization", authorizationValue);
        builder.addHeader("Content-Type", APPLICATION_JSON_UTF8_VALUE_STR);
        builder.addHeader("x-sfd-date", timestamp);
        builder.addHeader("x-sfd-nonce", String.valueOf(nonce));
    }

    private <T> T deSerialize(InputStream is, Type clazzType) throws IOException {
        try {
            return JSON.parseObject(is, clazzType);
        } finally {
            IOUtils.close(is);
        }
    }

    private <T> T deSerialize(String json, Type clazzType) throws IOException {
        return JSON.parseObject(json, clazzType);
    }

    private String serialize(Object object) {
        if (object == null) {
            return "";
        }

        return JSON.toJSONString(object);
    }

    private String toQueryString(LinkedHashMap<String, String> params) {
        if (params == null || params.size() == 0) {
            return "";
        }

        char parameterSeparator = '&';
        String charset = StandardCharsets.UTF_8.name();
        StringBuilder result = new StringBuilder();

        Iterator<String> it = params.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next();
            String value = params.get(key);
            String encodedName = encodeField(key, charset);
            String encodedValue = encodeField(value, charset);

            if (result.length() > 0) {
                result.append(parameterSeparator);
            }

            result.append(encodedName);
            if (encodedValue != null) {
                result.append("=");
                result.append(encodedValue);
            }
        }

        return result.toString();
    }

    private static String encodeField(String content, String charset) {
        if (StringUtils.isEmpty(content)) {
            return "";
        }

        try {
            return URLEncoder.encode(content, charset);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
