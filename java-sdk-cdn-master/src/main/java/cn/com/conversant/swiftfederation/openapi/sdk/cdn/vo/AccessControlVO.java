/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p/>
 * Created on 2018/5/18.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

/**
 * @author robin-laptop
 */
public class AccessControlVO extends PolicyBaseVO implements Serializable {
    //allow, deny, token
    private String type;

    private String subnet;

    private String location;

    public String getType() {
        return type;
    }

    public void setType(AccessControlType accessControlType) {
        this.type = accessControlType.getType();
    }

    public String getSubnet() {
        return subnet;
    }

    public void setSubnet(String subnet) {
        this.subnet = subnet;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static enum AccessControlType {
        ALLOW("allow"),
        DENY("deny"),
        TOKEN("token");

        private String type;

        AccessControlType(String type) {
            this.type = type;
        }

        public String getType() {
            return type;
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AccessControlVO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", matchType='").append(matchType).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", type='").append(type).append('\'');
        sb.append(", subnet='").append(subnet).append('\'');
        sb.append(", location='").append(location).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
