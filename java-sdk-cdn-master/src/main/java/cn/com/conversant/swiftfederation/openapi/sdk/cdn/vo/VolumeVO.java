/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop

 */
public class VolumeVO implements Serializable {
    private String domain;

    private List<TimestampAndValue> volumes;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<TimestampAndValue> getVolumes() {
        return volumes;
    }

    public void setVolumes(List<TimestampAndValue> volumes) {
        this.volumes = volumes;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("VolumeVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", volumes=").append(volumes);
        sb.append('}');
        return sb.toString();
    }
}
