/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p/>
 * Created on 2018/5/18.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

/**
 * @author robin-laptop
 */
public class CacheControlVO extends PolicyBaseVO implements Serializable {
    private String hostHeader;

    private String ttl;

    private boolean ignoreClientNoCache;

    private boolean ignoreOriginNoCache;

    private boolean ignoreQueryString;

    public String getHostHeader() {
        return hostHeader;
    }

    public void setHostHeader(String hostHeader) {
        this.hostHeader = hostHeader;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public boolean isIgnoreClientNoCache() {
        return ignoreClientNoCache;
    }

    public void setIgnoreClientNoCache(boolean ignoreClientNoCache) {
        this.ignoreClientNoCache = ignoreClientNoCache;
    }

    public boolean isIgnoreOriginNoCache() {
        return ignoreOriginNoCache;
    }

    public void setIgnoreOriginNoCache(boolean ignoreOriginNoCache) {
        this.ignoreOriginNoCache = ignoreOriginNoCache;
    }

    public boolean isIgnoreQueryString() {
        return ignoreQueryString;
    }

    public void setIgnoreQueryString(boolean ignoreQueryString) {
        this.ignoreQueryString = ignoreQueryString;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CacheControlVO{");
        sb.append("hostHeader='").append(hostHeader).append('\'');
        sb.append(", ttl='").append(ttl).append('\'');
        sb.append(", ignoreClientNoCache=").append(ignoreClientNoCache);
        sb.append(", ignoreOriginNoCache=").append(ignoreOriginNoCache);
        sb.append(", ignoreQueryString=").append(ignoreQueryString);
        sb.append(", id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", matchType='").append(matchType).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
