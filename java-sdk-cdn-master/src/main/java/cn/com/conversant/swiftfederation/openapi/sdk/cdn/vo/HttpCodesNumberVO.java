/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author robin-laptop
 */
public class HttpCodesNumberVO implements Serializable {
    private String domain;

    private List<HttpCodesNumber> httpCodes;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<HttpCodesNumber> getHttpCodes() {
        return httpCodes;
    }

    public void setHttpCodes(List<HttpCodesNumber> httpCodes) {
        this.httpCodes = httpCodes;
    }

    public static class HttpCodesNumber {
        private String timestamp;

        private Map<String, Long> httpCodeValues;

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public Map<String, Long> getHttpCodeValues() {
            return httpCodeValues;
        }

        public void setHttpCodeValues(Map<String, Long> httpCodeValues) {
            this.httpCodeValues = httpCodeValues;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("HttpCodesNumber{");
            sb.append("timestamp='").append(timestamp).append('\'');
            sb.append(", httpCodeValues=").append(httpCodeValues);
            sb.append('}');
            return sb.toString();
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HttpCodesNumberVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", httpCodes=").append(httpCodes);
        sb.append('}');
        return sb.toString();
    }
}
