/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop

 */
public class RequestHitRateVO implements Serializable {
    private String domain;

    private List<TimestampAndValue> reqHitRates;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<TimestampAndValue> getReqHitRates() {
        return reqHitRates;
    }

    public void setReqHitRates(List<TimestampAndValue> reqHitRates) {
        this.reqHitRates = reqHitRates;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestHitRateVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", reqHitRates=").append(reqHitRates);
        sb.append('}');
        return sb.toString();
    }
}
