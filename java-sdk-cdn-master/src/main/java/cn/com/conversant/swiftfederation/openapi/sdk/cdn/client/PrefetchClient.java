/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.PrefetchVO;
import com.alibaba.fastjson.TypeReference;

import java.util.*;

/**
 * @author robin-laptop
 */
public class PrefetchClient {
    private static RestClient restClient;

    public PrefetchClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.CDN_API_URL, accessKeyId, accessKeySecret);
    }

    public List<PrefetchVO> listPrefetches(long serviceId, int page) {
        String uri = String.format("/v1.0/services/%d/prefetches", serviceId);

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("page", String.valueOf(page));

        List<PrefetchVO> prefetchVOList = restClient.get(uri, params, new TypeReference<List<PrefetchVO>>() {
        }.getType());

        if (prefetchVOList == null) {
            return Collections.emptyList();
        }

        return prefetchVOList;
    }

    public List<PrefetchVO> listPrefetches(long serviceId) {
        return listPrefetches(1);
    }

    public void createPrefetches(long serviceId, String[] urls) {
        String uri = String.format("/v1.0/services/%d/prefetches", serviceId);

        Map<String, String[]> request = new HashMap<>();
        request.put("urls", urls);

        restClient.post(uri, request);
    }
}
