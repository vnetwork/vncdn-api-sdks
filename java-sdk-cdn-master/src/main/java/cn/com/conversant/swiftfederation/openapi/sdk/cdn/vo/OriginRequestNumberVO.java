/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop

 */
public class OriginRequestNumberVO implements Serializable {
    private String domain;

    private List<TimestampAndValue> originReqNums;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<TimestampAndValue> getOriginReqNums() {
        return originReqNums;
    }

    public void setOriginReqNums(List<TimestampAndValue> originReqNums) {
        this.originReqNums = originReqNums;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OriginRequestNumberVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", originReqNums=").append(originReqNums);
        sb.append('}');
        return sb.toString();
    }
}
