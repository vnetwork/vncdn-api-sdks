/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.CustomerVO;
import com.alibaba.fastjson.TypeReference;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author robin-laptop
 */
public class CustomersClient{
    private static RestClient restClient;

    public CustomersClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.BASE_API_URL, accessKeyId, accessKeySecret);
    }

    public List<CustomerVO> listCustomers() {
        String uri = String.format("/v1.1/customerChildren/");

        Map<String, List<CustomerVO>> customerList = restClient.get(uri, new TypeReference<Map<String, List<CustomerVO>>>() {
        }.getType());

        if (customerList == null) {
            return Collections.emptyList();
        }

        return customerList.get("customer");
    }

    public List<CustomerVO> listChildCustomers(long customerId) {
        String uri = String.format("/v1.1/customerChildren/%d", customerId);

        Map<String, List<CustomerVO>> customerList = restClient.get(uri, new TypeReference<Map<String, List<CustomerVO>>>() {
        }.getType());

        if (customerList == null) {
            return Collections.emptyList();
        }

        return customerList.get("customer");
    }

    public CustomerVO getCustomer(long customerId) {
        String uri = String.format("/v1.1/customer/%d", customerId);

        return restClient.get(uri, CustomerVO.class);
    }

    public CustomerVO createCustomer(CustomerVO customerVO) {
        String uri = "/v1.1/createCustomer/";

        return restClient.post(uri, customerVO, CustomerVO.class);
    }

    public void deleteCustomer(long customerId) {
        String uri = String.format("/v1.1/deleteCustomer/%d", customerId);

        restClient.delete(uri);
    }
}
