package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

public class TimestampAndValue implements Serializable {
    private String timestamp;
    private Object value;

    public TimestampAndValue(String timestamp, Object value) {
        this.timestamp = timestamp;
        this.value = value == null ? 0 : value;
    }

    public TimestampAndValue() {

    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TimestampAndValue{" +
                "timestamp='" + timestamp + '\'' +
                ", value=" + value +
                '}';
    }
}