/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.*;
import com.alibaba.fastjson.TypeReference;

import java.util.Collections;
import java.util.List;

/**
 * @author robin-laptop
 */
public class AnalyticsClient {
    private static RestClient restClient;

    public AnalyticsClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.CDN_API_URL, accessKeyId, accessKeySecret);
    }

    public List<BandwidthVO> getBandwidth(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.1/report/bandwidth";

        List<BandwidthVO> bandwidthVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<BandwidthVO>>() {
                }.getType());

        if (bandwidthVOList == null) {
            return Collections.emptyList();
        }

        return bandwidthVOList;
    }

    public List<TimestampAndValue> getCustomerBandwidth(long customerId, AnalyticsRequestVO analyticsRequestVO) {
        String uri = String.format("/v1.1/report/customers/%d/bandwidth", customerId);

        List<TimestampAndValue> bandwidthVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<TimestampAndValue>>() {
                }.getType());

        if (bandwidthVOList == null) {
            return Collections.emptyList();
        }

        return bandwidthVOList;
    }

    public List<BandwidthVO> getOriginBandwidth(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/origin_bandwidth";

        List<BandwidthVO> bandwidthVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<BandwidthVO>>() {
                }.getType());

        if (bandwidthVOList == null) {
            return Collections.emptyList();
        }

        return bandwidthVOList;
    }

    public List<VolumeVO> getVolume(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.1/report/volume";

        List<VolumeVO> volumeVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<VolumeVO>>() {
                }.getType());

        if (volumeVOList == null) {
            return Collections.emptyList();
        }

        return volumeVOList;
    }

    public List<TimestampAndValue> getCustomerVolume(long customerId, AnalyticsRequestVO analyticsRequestVO) {
        String uri = String.format("/v1.1/report/customers/%d/volume", customerId);

        List<TimestampAndValue> volumeVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<TimestampAndValue>>() {
                }.getType());

        if (volumeVOList == null) {
            return Collections.emptyList();
        }

        return volumeVOList;
    }

    public List<HitVolumeVO> getHitVolume(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/hit_volume";

        List<HitVolumeVO> hitVolumeVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<HitVolumeVO>>() {
                }.getType());

        if (hitVolumeVOList == null) {
            return Collections.emptyList();
        }

        return hitVolumeVOList;
    }

    public List<RequestNumberVO> getRequestNumber(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/request_number";

        List<RequestNumberVO> requestNumberVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<RequestNumberVO>>() {
                }.getType());

        if (requestNumberVOList == null) {
            return Collections.emptyList();
        }

        return requestNumberVOList;
    }

    public List<HitRequestNumberVO> getHitRequestNumber(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/hit_request_number";

        List<HitRequestNumberVO> hitRequestNumberVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<HitRequestNumberVO>>() {
                }.getType());

        if (hitRequestNumberVOList == null) {
            return Collections.emptyList();
        }

        return hitRequestNumberVOList;
    }

    public List<OriginRequestNumberVO> getOriginRequestNumber(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/origin_request_number";

        List<OriginRequestNumberVO> originRequestNumberVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<OriginRequestNumberVO>>() {
                }.getType());

        if (originRequestNumberVOList == null) {
            return Collections.emptyList();
        }

        return originRequestNumberVOList;
    }

    public List<RequestHitRateVO> getRequestHitRate(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/request_hit_rate";

        List<RequestHitRateVO> requestHitRateVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<RequestHitRateVO>>() {
                }.getType());

        if (requestHitRateVOList == null) {
            return Collections.emptyList();
        }

        return requestHitRateVOList;
    }

    public List<HttpCodesNumberVO> getHttpCodesNumber(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/http_codes";

        List<HttpCodesNumberVO> httpCodesNumberVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<HttpCodesNumberVO>>() {
                }.getType());

        if (httpCodesNumberVOList == null) {
            return Collections.emptyList();
        }

        return httpCodesNumberVOList;
    }

    public List<HttpCodesNumberVO> getOriginHttpCodesNumber(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/report/origin_http_codes";

        List<HttpCodesNumberVO> httpCodesNumberVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<HttpCodesNumberVO>>() {
                }.getType());

        if (httpCodesNumberVOList == null) {
            return Collections.emptyList();
        }

        return httpCodesNumberVOList;
    }
}
