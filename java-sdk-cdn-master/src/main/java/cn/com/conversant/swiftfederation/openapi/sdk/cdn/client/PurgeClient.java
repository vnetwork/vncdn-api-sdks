/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.PurgeVO;
import com.alibaba.fastjson.TypeReference;

import java.util.Collections;
import java.util.List;

/**
 * @author robin-laptop
 */
public class PurgeClient {
    private static RestClient restClient;

    public PurgeClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.CDN_API_URL, accessKeyId, accessKeySecret);
    }

    public List<PurgeVO> listPurges(long serviceId) {
        String uri = String.format("/v1.1/services/%d/purges", serviceId);

        List<PurgeVO> purgeVOList = restClient.get(uri, new TypeReference<List<PurgeVO>>() {
        }.getType());

        if (purgeVOList == null) {
            return Collections.emptyList();
        }

        return purgeVOList;
    }

    public void createPurge(long serviceId, PurgeVO purgeVO) {
        String uri = String.format("/v1.1/services/%d/purges", serviceId);

        restClient.post(uri, purgeVO);
    }
}
