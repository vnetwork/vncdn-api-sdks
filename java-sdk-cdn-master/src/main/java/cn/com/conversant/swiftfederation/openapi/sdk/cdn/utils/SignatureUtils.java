package cn.com.conversant.swiftfederation.openapi.sdk.cdn.utils;


import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;

/**
 * User: Robin
 * Date: 2018/11/26
 * Time: 12:17
 * Description:
 */
public class SignatureUtils {
    private static final String HMAC_SHA256 = "HmacSHA256";

    private static String signString(String signKey, String signingString, String signAlgorithm) {
        try {
            Mac mac = Mac.getInstance(signAlgorithm);
            mac.init(new SecretKeySpec(signKey.getBytes(StandardCharsets.UTF_8), signAlgorithm));
            byte[] signedBytes = mac.doFinal(signingString.getBytes(StandardCharsets.UTF_8));
            return Hex.encodeHexString(signedBytes);
        } catch (Exception e) {
            throw new RuntimeException("Fail to generate the signature", e);
        }
    }

    public static String hexHmacSha256(String signKey, String signingString) {
        return signString(signKey, signingString, HMAC_SHA256);
    }

    public static int getNonce() {
        return (int) ((Math.random() * 9 + 1) * 10000);
    }
}
