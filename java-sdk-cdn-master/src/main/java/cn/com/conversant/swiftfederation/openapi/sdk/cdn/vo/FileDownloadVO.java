/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/09/11
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

/**
 * @author: Robin
 */
public class FileDownloadVO implements Serializable {
    private Long id;

    private String name;

    private Long customerId;

    private String originUrl;

    private String deliveryDomain;

    private int originAccountId;

    private String webDavUrl;

    private String deliveryEndpoint;

    private String diskUsage;

    private Boolean streamingService;

    private Boolean active;

    private String tokenSecret;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getOriginUrl() {
        return originUrl;
    }

    public void setOriginUrl(String originUrl) {
        this.originUrl = originUrl;
    }

    public Boolean getStreamingService() {
        return streamingService;
    }

    public void setStreamingService(Boolean streamingService) {
        this.streamingService = streamingService;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDeliveryDomain() {
        return deliveryDomain;
    }

    public void setDeliveryDomain(String deliveryDomain) {
        this.deliveryDomain = deliveryDomain;
    }

    public int getOriginAccountId() {
        return originAccountId;
    }

    public void setOriginAccountId(int originAccountId) {
        this.originAccountId = originAccountId;
    }

    public String getWebDavUrl() {
        return webDavUrl;
    }

    public void setWebDavUrl(String webDavUrl) {
        this.webDavUrl = webDavUrl;
    }

    public String getDeliveryEndpoint() {
        return deliveryEndpoint;
    }

    public void setDeliveryEndpoint(String deliveryEndpoint) {
        this.deliveryEndpoint = deliveryEndpoint;
    }

    public String getDiskUsage() {
        return diskUsage;
    }

    public void setDiskUsage(String diskUsage) {
        this.diskUsage = diskUsage;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FileDownloadVO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", customerId=").append(customerId);
        sb.append(", originUrl='").append(originUrl).append('\'');
        sb.append(", deliveryDomain='").append(deliveryDomain).append('\'');
        sb.append(", originAccountId=").append(originAccountId);
        sb.append(", webDavUrl='").append(webDavUrl).append('\'');
        sb.append(", deliveryEndpoint='").append(deliveryEndpoint).append('\'');
        sb.append(", diskUsage='").append(diskUsage).append('\'');
        sb.append(", streamingService=").append(streamingService);
        sb.append(", active=").append(active);
        sb.append(", tokenSecret='").append(tokenSecret).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
