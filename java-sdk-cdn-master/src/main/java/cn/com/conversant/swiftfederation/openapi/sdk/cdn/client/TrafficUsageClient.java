/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.*;
import com.alibaba.fastjson.TypeReference;

import java.util.Collections;
import java.util.List;

/**
 * @author robin-laptop
 */
public class TrafficUsageClient {
    private static RestClient restClient;

    public TrafficUsageClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.CDN_API_URL, accessKeyId, accessKeySecret);
    }

    public List<BandwidthVO> getBandwidthUsage(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/usage/bandwidth";

        List<BandwidthVO> bandwidthVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<BandwidthVO>>() {
                }.getType());

        if (bandwidthVOList == null) {
            return Collections.emptyList();
        }

        return bandwidthVOList;
    }

    public List<VolumeVO> getVolumeUsage(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/usage/volume";

        List<VolumeVO> volumeVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<VolumeVO>>() {
                }.getType());

        if (volumeVOList == null) {
            return Collections.emptyList();
        }

        return volumeVOList;
    }

    public List<RequestNumberVO> getRequestNumberUsage(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.0/usage/request_number";

        List<RequestNumberVO> requestNumberVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<RequestNumberVO>>() {
                }.getType());

        if (requestNumberVOList == null) {
            return Collections.emptyList();
        }

        return requestNumberVOList;
    }
}
