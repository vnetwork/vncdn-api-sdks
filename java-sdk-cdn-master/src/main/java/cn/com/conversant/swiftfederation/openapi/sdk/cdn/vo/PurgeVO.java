/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

/**
 * @author robin-laptop
 */
public class PurgeVO implements Serializable {
    private String urlPath;

    private long expireTimestamp;

    public String getUrlPath() {
        return urlPath;
    }

    public void setUrlPath(String urlPath) {
        this.urlPath = urlPath;
    }

    public long getExpireTimestamp() {
        return expireTimestamp;
    }

    public void setExpireTimestamp(long expireTimestamp) {
        this.expireTimestamp = expireTimestamp;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PurgeVO{");
        sb.append("urlPath='").append(urlPath).append('\'');
        sb.append(", expireTimestamp=").append(expireTimestamp);
        sb.append('}');
        return sb.toString();
    }
}
