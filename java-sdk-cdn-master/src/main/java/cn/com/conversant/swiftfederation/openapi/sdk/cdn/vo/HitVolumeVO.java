/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop

 */
public class HitVolumeVO implements Serializable {
    private String domain;

    private List<TimestampAndValue> hitVolumes;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<TimestampAndValue> getHitVolumes() {
        return hitVolumes;
    }

    public void setHitVolumes(List<TimestampAndValue> hitVolumes) {
        this.hitVolumes = hitVolumes;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HitVolumeVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", hitVolumes=").append(hitVolumes);
        sb.append('}');
        return sb.toString();
    }
}
