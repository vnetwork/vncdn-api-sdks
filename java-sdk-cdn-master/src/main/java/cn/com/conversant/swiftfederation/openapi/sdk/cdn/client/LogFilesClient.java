/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.AnalyticsRequestVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.LogFilesVO;
import com.alibaba.fastjson.TypeReference;

import java.util.Collections;
import java.util.List;

/**
 * @author robin-laptop
 */
public class LogFilesClient {
    private static RestClient restClient;

    public LogFilesClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.CDN_API_URL, accessKeyId, accessKeySecret);
    }

    public List<LogFilesVO> getLogFiles(AnalyticsRequestVO analyticsRequestVO) {
        String uri = "/v1.1/log/list";

        List<LogFilesVO> logFilesVOList = restClient.post(uri, analyticsRequestVO,
                new TypeReference<List<LogFilesVO>>() {
                }.getType());

        if (logFilesVOList == null) {
            return Collections.emptyList();
        }

        return logFilesVOList;
    }
}
