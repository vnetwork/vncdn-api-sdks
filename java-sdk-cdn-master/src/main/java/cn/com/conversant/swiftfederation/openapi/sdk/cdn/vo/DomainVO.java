/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/09/11
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

/**
 * @author: Robin
 */
public class DomainVO implements Serializable {
    private Long id;

    private String name;

    private Long customerId;

    private String originUrl;

    private String deliveryDomain;

    private Boolean streamingService;

    private Boolean active;

    private String tokenSecret;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getOriginUrl() {
        return originUrl;
    }

    public void setOriginUrl(String originUrl) {
        this.originUrl = originUrl;
    }

    public Boolean getStreamingService() {
        return streamingService;
    }

    public void setStreamingService(Boolean streamingService) {
        this.streamingService = streamingService;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getDeliveryDomain() {
        return deliveryDomain;
    }

    public void setDeliveryDomain(String deliveryDomain) {
        this.deliveryDomain = deliveryDomain;
    }

    public String getTokenSecret() {
        return tokenSecret;
    }

    public void setTokenSecret(String tokenSecret) {
        this.tokenSecret = tokenSecret;
    }

    @Override
    public String toString() {
        return "DomainVO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", customerId=" + customerId +
                ", originUrl='" + originUrl + '\'' +
                ", streamingService=" + streamingService +
                ", active=" + active +
                '}';
    }
}
