/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p/>
 * Created on 2018/5/18.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

/**
 * @author robin-laptop
 */
public class RedirectionVO extends PolicyBaseVO implements Serializable {
    private String redirectionURL;

    private int statusCode;

    public String getRedirectionURL() {
        return redirectionURL;
    }

    public void setRedirectionURL(String redirectionURL) {
        this.redirectionURL = redirectionURL;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RedirectionVO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", matchType='").append(matchType).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append(", redirectionURL='").append(redirectionURL).append('\'');
        sb.append(", statusCode=").append(statusCode);
        sb.append('}');
        return sb.toString();
    }
}
