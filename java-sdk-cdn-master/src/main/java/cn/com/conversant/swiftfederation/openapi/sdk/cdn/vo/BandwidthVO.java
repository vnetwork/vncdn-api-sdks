/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop

 */
public class BandwidthVO implements Serializable {
    private String domain;

    private List<TimestampAndValue> bandwidths;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<TimestampAndValue> getBandwidths() {
        return bandwidths;
    }

    public void setBandwidths(List<TimestampAndValue> bandwidths) {
        this.bandwidths = bandwidths;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BandwidthVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", bandwidths=").append(bandwidths);
        sb.append('}');
        return sb.toString();
    }
}
