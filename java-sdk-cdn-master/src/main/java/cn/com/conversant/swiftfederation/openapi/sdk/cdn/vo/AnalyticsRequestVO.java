/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @author robin-laptop

 */
public class AnalyticsRequestVO implements Serializable {
    private String[] domains;

    private String startTime;

    private String endTime;

    private boolean fillFixedTime;

    private String interval;

    public String[] getDomains() {
        return domains;
    }

    public void setDomains(String[] domains) {
        this.domains = domains;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public boolean isFillFixedTime() {
        return fillFixedTime;
    }

    public void setFillFixedTime(boolean fillFixedTime) {
        this.fillFixedTime = fillFixedTime;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AnalyticsRequestVO{");
        sb.append("domains=").append(Arrays.toString(domains));
        sb.append(", startTime='").append(startTime).append('\'');
        sb.append(", endTime='").append(endTime).append('\'');
        sb.append(", fillFixedTime=").append(fillFixedTime);
        sb.append(", interval='").append(interval).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
