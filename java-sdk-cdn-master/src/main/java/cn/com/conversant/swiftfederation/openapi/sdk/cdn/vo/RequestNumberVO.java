/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop

 */
public class RequestNumberVO implements Serializable {
    private String domain;

    private List<TimestampAndValue> reqNumbers;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<TimestampAndValue> getReqNumbers() {
        return reqNumbers;
    }

    public void setReqNumbers(List<TimestampAndValue> reqNumbers) {
        this.reqNumbers = reqNumbers;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("RequestNumberVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", reqNumbers=").append(reqNumbers);
        sb.append('}');
        return sb.toString();
    }
}
