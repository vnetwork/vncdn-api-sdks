/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.client;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.Consts;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.RestClient;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.AccessControlVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.CacheControlVO;
import cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo.RedirectionVO;
import com.alibaba.fastjson.TypeReference;

import java.util.Collections;
import java.util.List;

/**
 * @author robin-laptop
 */
public class PolicyClient {
    private static RestClient restClient;

    public PolicyClient(String accessKeyId, String accessKeySecret) {
        restClient = new RestClient(Consts.CDN_API_URL, accessKeyId, accessKeySecret);
    }

    public List<AccessControlVO> listAccessControls(long serviceId) {
        String uri = String.format("/v1.0/services/%d/access_controls", serviceId);

        List<AccessControlVO> accessControlVOList = restClient.get(uri, new TypeReference<List<AccessControlVO>>() {
        }.getType());

        if (accessControlVOList == null) {
            return Collections.emptyList();
        }

        return accessControlVOList;
    }

    public AccessControlVO createAccessControl(long serviceId, AccessControlVO accessControlVO) {
        String uri = String.format("/v1.0/services/%d/access_controls", serviceId);

        return restClient.post(uri, accessControlVO, new TypeReference<AccessControlVO>() {
        }.getType());
    }

    public void editAccessControl(long serviceId, long policyId, AccessControlVO accessControlVO) {
        String uri = String.format("/v1.0/services/%d/access_controls/%d", serviceId, policyId);

        restClient.put(uri, accessControlVO);
    }

    public void deleteAccessControl(long serviceId, long policyId) {
        String uri = String.format("/v1.0/services/%d/access_controls/%d", serviceId, policyId);

        restClient.delete(uri);
    }

    public List<CacheControlVO> listCacheControls(long serviceId) {
        String uri = String.format("/v1.0/services/%d/cache_controls", serviceId);

        List<CacheControlVO> cacheControlVOList = restClient.get(uri, new TypeReference<List<CacheControlVO>>() {
        }.getType());

        if (cacheControlVOList == null) {
            return Collections.emptyList();
        }

        return cacheControlVOList;
    }

    public CacheControlVO createCacheControl(long serviceId, CacheControlVO cacheControlVO) {
        String uri = String.format("/v1.0/services/%d/cache_controls", serviceId);

        return restClient.post(uri, cacheControlVO, new TypeReference<CacheControlVO>() {
        }.getType());
    }

    public void editCacheControl(long serviceId, long policyId, CacheControlVO cacheControlVO) {
        String uri = String.format("/v1.0/services/%d/cache_controls/%d", serviceId, policyId);

        restClient.put(uri, cacheControlVO);
    }

    public void deleteCacheControl(long serviceId, long policyId) {
        String uri = String.format("/v1.0/services/%d/cache_controls/%d", serviceId, policyId);

        restClient.delete(uri);
    }


    public List<RedirectionVO> listRedirections(long serviceId) {
        String uri = String.format("/v1.0/services/%d/redirections", serviceId);

        List<RedirectionVO> redirectionVOList = restClient.get(uri, new TypeReference<List<RedirectionVO>>() {
        }.getType());

        if (redirectionVOList == null) {
            return Collections.emptyList();
        }

        return redirectionVOList;
    }

    public RedirectionVO createRedirection(long serviceId, RedirectionVO redirectionVO) {
        String uri = String.format("/v1.0/services/%d/redirections", serviceId);

        return restClient.post(uri, redirectionVO, new TypeReference<RedirectionVO>() {
        }.getType());
    }

    public void editRedirection(long serviceId, long policyId, RedirectionVO redirectionVO) {
        String uri = String.format("/v1.0/services/%d/redirections/%d", serviceId, policyId);

        restClient.put(uri, redirectionVO);
    }

    public void deleteRedirection(long serviceId, long policyId) {
        String uri = String.format("/v1.0/services/%d/redirections/%d", serviceId, policyId);

        restClient.delete(uri);
    }
}
