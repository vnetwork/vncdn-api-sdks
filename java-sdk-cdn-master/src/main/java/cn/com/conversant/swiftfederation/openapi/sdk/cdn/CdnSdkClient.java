/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

import cn.com.conversant.swiftfederation.openapi.sdk.cdn.client.*;

/**
 * @author robin-laptop
 */
public class CdnSdkClient {
    private static String accessKeyId;

    private static String accessKeySecret;

    private static CdnSdkClient cdnSdkClient;

    public CdnSdkClient() {

    }

    public static CdnSdkClient build(String accessKeyId, String accessKeySecret) {
        synchronized (CdnSdkClient.class) {
            if (cdnSdkClient == null) {
                synchronized (CdnSdkClient.class) {
                    CdnSdkClient.accessKeyId = accessKeyId;
                    CdnSdkClient.accessKeySecret = accessKeySecret;

                    cdnSdkClient = new CdnSdkClient();
                }
            }
        }

        return cdnSdkClient;
    }

    public CustomersClient getCustomersClient() {
        return new CustomersClient(accessKeyId, accessKeySecret);
    }

    public ServicesClient getServicesClient() {
        return new ServicesClient(accessKeyId, accessKeySecret);
    }

    public AnalyticsClient getAnalyticsClient() {
        return new AnalyticsClient(accessKeyId, accessKeySecret);
    }

    public LogFilesClient getLogFilesClient() {
        return new LogFilesClient(accessKeyId, accessKeySecret);
    }

    public PrefetchClient getPrefetchClient() {
        return new PrefetchClient(accessKeyId, accessKeySecret);
    }

    public PurgeClient getPurgeClient() {
        return new PurgeClient(accessKeyId, accessKeySecret);
    }

    public PolicyClient getPolicyClient() {
        return new PolicyClient(accessKeyId, accessKeySecret);
    }

    public TrafficUsageClient getTrafficUsageClient() {
        return new TrafficUsageClient(accessKeyId, accessKeySecret);
    }
}
