/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/26.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn;

/**
 * @author robin-laptop

 */
public interface Consts {
    String BASE_API_URL = "https://base-api.swiftfederation.com";

    String CDN_API_URL = "https://cdn-api.swiftfederation.com";
}
