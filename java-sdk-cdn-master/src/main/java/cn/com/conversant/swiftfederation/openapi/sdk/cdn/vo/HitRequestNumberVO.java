/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;
import java.util.List;

/**
 * @author robin-laptop
 */
public class HitRequestNumberVO implements Serializable {
    private String domain;

    private List<HitReqNumber> hitReqNumbers;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public List<HitReqNumber> getHitReqNumbers() {
        return hitReqNumbers;
    }

    public void setHitReqNumbers(List<HitReqNumber> hitReqNumbers) {
        this.hitReqNumbers = hitReqNumbers;
    }

    public static class HitReqNumber {
        private String timestamp;

        private long request;

        private long hit;

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public long getRequest() {
            return request;
        }

        public void setRequest(long request) {
            this.request = request;
        }

        public long getHit() {
            return hit;
        }

        public void setHit(long hit) {
            this.hit = hit;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("HitReqNumber{");
            sb.append("timestamp='").append(timestamp).append('\'');
            sb.append(", request=").append(request);
            sb.append(", hit=").append(hit);
            sb.append('}');
            return sb.toString();
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("HitRequestNumberVO{");
        sb.append("domain='").append(domain).append('\'');
        sb.append(", hitReqNumbers=").append(hitReqNumbers);
        sb.append('}');
        return sb.toString();
    }
}
