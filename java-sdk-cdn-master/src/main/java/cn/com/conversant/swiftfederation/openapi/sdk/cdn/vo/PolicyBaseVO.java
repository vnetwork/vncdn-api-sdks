/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/11/27.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

/**
 * TODO
 *
 * @author robin-laptop
 */
public class PolicyBaseVO {
    protected long id;

    protected String name;

    //'regex' or 'prefix'
    protected String matchType;

    protected String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMatchType() {
        return matchType;
    }

    public void setMatchType(MatchType matchType) {
        this.matchType = matchType.getMatchType();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static enum MatchType {
        PREFIX("prefix"),
        REGEX("regex");

        private String matchType;

        MatchType(String matchType) {
            this.matchType = matchType;
        }

        public String getMatchType() {
            return matchType;
        }
    }
}
