/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2018/09/11
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.vo;

import java.io.Serializable;

/**
 * @author: Robin
 */
public class FileDownloadRequestVO implements Serializable {
    private Long id;

    private String name;

    private String ftpPassword;

    private Long customerId;

    private Boolean streamingService;

    private Boolean active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFtpPassword() {
        return ftpPassword;
    }

    public void setFtpPassword(String ftpPassword) {
        this.ftpPassword = ftpPassword;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Boolean getStreamingService() {
        return streamingService;
    }

    public void setStreamingService(Boolean streamingService) {
        this.streamingService = streamingService;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("FileDownloadVO{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", customerId=").append(customerId);
        sb.append(", streamingService=").append(streamingService);
        sb.append(", active=").append(active);
        sb.append('}');
        return sb.toString();
    }
}
