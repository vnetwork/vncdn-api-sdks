/**
 * Copyright (c) 2012 Conversant Solutions. All rights reserved.
 * <p>
 * Created on 2/19/13.
 */
package cn.com.conversant.swiftfederation.openapi.sdk.cdn.utils;


import org.apache.commons.io.output.StringBuilderWriter;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * IO Stream Utility
 *
 * @author robin.ye
 */
public class IOUtils {
    public static final int BYTE_BUFFER_SIZE = 4096;

    public static final int EOF = -1;

    public static void close(final Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (final IOException ioe) {
            // skip
        }
    }

    public static String toString(InputStream is) {
        if (is == null) {
            return "";
        }

        try (
                final InputStreamReader in = new InputStreamReader(is, StandardCharsets.UTF_8);
                final StringBuilderWriter sbw = new StringBuilderWriter();
        ) {
            char[] buffer = new char[BYTE_BUFFER_SIZE];
            int read;
            while ((read = in.read(buffer)) != EOF) {
                sbw.write(buffer, 0, read);
            }

            return sbw.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
