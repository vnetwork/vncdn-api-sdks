# -*- coding: UTF-8 -*-
from api.http_util import HttpUtil


class Domain:

    access_key_id = ''
    access_key_secret = ''

    def __init__(self, access_key_id, access_key_secret):
        self.access_key_id = access_key_id
        self.access_key_secret = access_key_secret

    def list_domain(self, customer_id):
        uri = '/v1.0/customers/%s/domains' % customer_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def create_domain(self, customer_id, name, origin_url):
        uri = '/v1.0/domains/'
        request_body = '{\
            "name": "%s",\
            "customerId": %d,\
            "originUrl": "%s"\
        }' % (name, customer_id, origin_url)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def edit_domain(self, service_id, origin_url, enable_stream, status):
        uri = '/v1.0/domains/%s' % service_id
        request_body = '{\
                        "originUrl": "%s",\
                        "streamingService": "%s",\
                        "active": "%s"\
                        }' % (origin_url, enable_stream, status)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_PUT)

    def update_domain_status(self, service_id, status):
        uri = '/v1.0/domains/%s/status' % service_id
        request_body = '{\
                        "active": "%s"\
                        }' % status
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_PUT)

    def delete_domain(self, service_id):
        uri = '/v1.0/domains/%s' % service_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_DELETE)

    def get_domain(self, service_id):
        uri = '/v1.0/domains/%s' % service_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def list_lfd_domain(self, customer_id):
        uri = '/v1.0/customers/%s/filedownloads' % customer_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def create_lfd_domain(self, customer_id, name, ftp_password):
        uri = '/v1.0/filedownloads/'
        request_body = '{\
            "name": "%s",\
            "customerId": %d,\
            "ftpPassword": "%s"\
        }' % (name, customer_id, ftp_password)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def edit_lfd_domain(self, service_id, ftp_password, enable_stream, status):
        uri = '/v1.0/filedownloads/%s' % service_id
        request_body = '{\
                        "ftpPassword": "%s",\
                        "streamingService": "%s",\
                        "active": "%s"\
                        }' % (ftp_password, enable_stream, status)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_PUT)

    def update_lfd_domain_status(self, service_id, status):
        uri = '/v1.0/filedownloads/%s/status' % service_id
        request_body = '{\
                        "active": "%s"\
                        }' % status
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_PUT)

    def delete_lfd_domain(self, service_id):
        uri = '/v1.0/filedownloads/%s' % service_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_DELETE)

    def get_lfd_domain(self, service_id):
        uri = '/v1.0/filedownloads/%s' % service_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

