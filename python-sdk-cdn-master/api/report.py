# -*- coding: UTF-8 -*-
from api.http_util import HttpUtil


class Report:

    access_key_id = ''
    access_key_secret = ''

    def __init__(self, access_key_id, access_key_secret):
        self.access_key_id = access_key_id
        self.access_key_secret = access_key_secret

    def get_bandwidth_v10(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/bandwidth"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_hit_volume(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/hit_volume"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_origin_bandwidth(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/origin_bandwidth"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_request_hit_number(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/hit_request_number"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_origin_request_number(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/origin_request_number"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_request_hit_rate(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/request_hit_rate"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_request_number(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/request_number"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_http_codes(self, domains, start_time, end_time):
        uri = "/v1.0/report/http_codes"
        request_body = Report.populate_req_body(domains, start_time, end_time)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_volume(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.0/report/volume"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_volume_v11(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.1/report/volume"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_customer_volume(self,customer_id, start_time, end_time):
        # only can can day interval
        uri = "/v1.1/report/customers/%s/volume" % customer_id
        request_body = Report.populate_req_body("[]", start_time, end_time, True, "day")
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_customer_bandwidth(self, customerId, start_time, end_time):
        # only can can day interval
        uri = "/v1.1/report/customers/%s/bandwidth" % customerId
        request_body = Report.populate_req_body("[]", start_time, end_time, fill_fixed_time=True, interval='day')
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def list_logs(self, domains, start_time, end_time):
        uri = "/v1.0/log/list"
        request_body = Report.populate_req_body(domains, start_time, end_time)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def list_logs_v11(self, domains, start_time, end_time):
        uri = "/v1.1/log/list"
        request_body = Report.populate_req_body(domains, start_time, end_time)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def get_bandwidth_v11(self, domains, start_time, end_time, fill_fix, interval):
        uri = "/v1.1/report/bandwidth"
        request_body = Report.populate_req_body(domains, start_time, end_time, fill_fix, interval)
        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    @staticmethod
    def populate_req_body(domains, start_time, end_time, fill_fixed_time=False, interval='minute'):
        return '{"domains":%s,"startTime":"%s","endTime":"%s","fillFixedTime":"%s","interval":"%s"}' \
               % (domains, start_time, end_time, fill_fixed_time, interval)


