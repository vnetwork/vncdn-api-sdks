# -*- coding: UTF-8 -*-
from random import randint
import urllib
import hmac
import hashlib

import requests

import time


class HttpUtil:
    HTTP_GET = 'GET'
    HTTP_POST = 'POST'
    HTTP_PUT = 'PUT'
    HTTP_DELETE = 'DELETE'

    @staticmethod
    def request_api(uri, request_body, access_key_id, access_key_secret, method=HTTP_POST, api_server="https://cdn-api.swiftfederation.com"):
        request_method = method

        request_timestamp = time.strftime("%Y%m%dT%H%M%SZ", time.localtime(time.time() + time.timezone))
        nonce = randint(10000, 99999)

        if not request_body:
            request_body = ''

        if request_method == HttpUtil.HTTP_GET:
            request_body = urllib.parse.urlencode(request_body, doseq=True)

        signing_string = "%s\n%s\n%s\n%d\n%s\n%s" % (request_method.upper(), uri, request_timestamp, nonce,
                                                     access_key_id, request_body)

        signature = HttpUtil.hex_hmac_sha256(access_key_secret, signing_string)

        authorization_val = "HMAC-SHA256 " + access_key_id + ":" + signature

        url = "%s%s" % (api_server, uri)

        headers = dict()
        headers['Authorization'] = authorization_val
        headers['Content-Type'] = "application/json; charset=utf-8"
        headers['x-sfd-date'] = request_timestamp
        headers['x-sfd-nonce'] = str(nonce)

        ret = None
        if request_method == HttpUtil.HTTP_POST:
            ret = requests.post(url, headers=headers, data=request_body)
        elif request_method == HttpUtil.HTTP_GET:
            ret = requests.get(url, headers=headers, params=request_body)
        elif request_method == HttpUtil.HTTP_PUT:
            ret = requests.put(url, headers=headers, data=request_body)
        elif request_method == HttpUtil.HTTP_DELETE:
            ret = requests.delete(url, headers=headers, data=request_body)
        else:
            print("Invalid request method : %s" % request_method)
            return ret

        rs = ret.text

        return rs

    @staticmethod
    def hex_hmac_sha256(sign_key, signing_string):
        signed_bytes = hmac.new(bytes(sign_key, 'utf-8'), bytes(signing_string, 'utf-8'),
                                digestmod=hashlib.sha256).hexdigest()
        return signed_bytes
