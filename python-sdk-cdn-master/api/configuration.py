# -*- coding: UTF-8 -*-
from api.http_util import HttpUtil


class Configuration:

    access_key_id = ''
    access_key_secret = ''

    def __init__(self, access_key_id, access_key_secret):
        self.access_key_id = access_key_id
        self.access_key_secret = access_key_secret

    def list_purges(self, service_id, service_type):
        uri = '/v1.0/services/%s/purges' % service_id
        request_body = '{\
            "serviceType": "%s"\
        }' % service_type
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def create_purge(self, service_id, url_path, expire_timestamp, service_type):
        uri = '/v1.0/services/%s/purges' % service_id
        request_body = '{\
            "urlPath": "%s",\
            "expireTimestamp": %d,\
            "serviceType": "%s"\
        }' % (url_path, expire_timestamp, service_type)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def list_prefetches_page(self, service_id, page):
        uri = '/v1.0/services/%s/prefetches' % service_id
        request_body = {'page': page}
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def create_prefetch(self, service_id, urls):
        uri = '/v1.0/services/%s/prefetches' % service_id
        request_body = '{\
        "urls": %s\
        }' % urls

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def list_access_controls(self, service_id):
        uri = '/v1.0/services/%s/access_controls' % service_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def create_access_control(self, service_id, name, type, match_type, url, subnet, location):
        uri = '/v1.0/services/%s/access_controls' % service_id
        request_body = '{\
                        "name": "%s",\
                        "type": "%s",\
                        "matchType": "%s",\
                        "url": "%s",\
                        "subnet": "%s",\
                        "location": "%s"\
                        }' % (name, type, match_type, url, subnet, location)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def edit_access_control(self, service_id, policy_id, name, type, match_type, url, subnet, location):
        uri = '/v1.0/services/%s/access_controls/%s' % (service_id, policy_id)
        request_body = '{\
                        "name": "%s",\
                        "type": "%s",\
                        "matchType": "%s",\
                        "url": "%s",\
                        "subnet": "%s",\
                        "location": "%s"\
                        }' % (name, type, match_type, url, subnet, location)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_PUT)

    def delete_access_control(self, service_id, policy_id):
        uri = '/v1.0/services/%s/access_controls/%s' % (service_id, policy_id)

        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_DELETE)

    def list_cache_controls(self, service_id):
        uri = '/v1.0/services/%s/cache_controls' % service_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def create_cache_control(self, service_id, name, match_type, url, host_header, ttl, ignore_client_no_cache, ignore_origin_no_cache, ignore_query_string):
        uri = '/v1.0/services/%s/cache_controls' % service_id
        request_body = '{\
                        "name": "%s",\
                        "matchType": "%s",\
                        "url": "%s",\
                        "hostHeader": "%s",\
                        "ttl": %d,\
                        "ignoreClientNoCache": "%s",\
                        "ignoreOriginNoCache": "%s",\
                        "ignoreQueryString": "%s"\
                    }' %(name, match_type, url, host_header, ttl, ignore_client_no_cache, ignore_origin_no_cache, ignore_query_string)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def edit_cache_control(self, service_id, policy_id, name, match_type, url, host_header, ttl, ignore_client_no_cache, ignore_origin_no_cache, ignore_query_string):
        uri = '/v1.0/services/%s/cache_controls/%s' % (service_id, policy_id)
        request_body = '{\
                        "name": "%s",\
                        "matchType": "%s",\
                        "url": "%s",\
                        "hostHeader": "%s",\
                        "ttl": %d,\
                        "ignoreClientNoCache": "%s",\
                        "ignoreOriginNoCache": "%s",\
                        "ignoreQueryString": "%s"\
                    }' % (name, match_type, url, host_header, ttl, ignore_client_no_cache, ignore_origin_no_cache, ignore_query_string)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_PUT)

    def delete_cache_control(self, service_id, policy_id):
        uri = '/v1.0/services/%s/cache_controls/%s' % (service_id, policy_id)

        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_DELETE)

    def list_redirections(self, service_id):
        uri = '/v1.0/services/%s/redirections' % service_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET)

    def create_redirection(self, service_id, name, match_type, url, redirect_url, status_code):
        uri = '/v1.0/services/%s/redirections' % service_id
        request_body = '{\
                        "name": "%s",\
                        "matchType": "%s",\
                        "url": "%s",\
                        "redirectionURL": "%s",\
                        "statusCode": %d\
                    }' % (name, match_type, url, redirect_url, status_code)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST)

    def edit_redirection(self, service_id, policy_id, name, match_type, url, redirect_url, status_code):
        uri = '/v1.0/services/%s/redirections/%s' % (service_id, policy_id)
        request_body = '{\
                        "name": "%s",\
                        "matchType": "%s",\
                        "url": "%s",\
                        "redirectionURL": "%s",\
                        "statusCode": %d\
                    }' % (name, match_type, url, redirect_url, status_code)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_PUT)

    def delete_redirection(self, service_id, policy_id):
        uri = '/v1.0/services/%s/redirections/%s' % (service_id, policy_id)

        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_DELETE)
