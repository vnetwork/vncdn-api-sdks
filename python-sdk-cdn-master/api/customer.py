# -*- coding: UTF-8 -*-
from api.http_util import HttpUtil


class Customer:

    access_key_id = ''
    access_key_secret = ''
    api_server = 'https://base-api.swiftfederation.com'

    def __init__(self, access_key_id, access_key_secret):
        self.access_key_id = access_key_id
        self.access_key_secret = access_key_secret

    def get_customer(self, customer_id):
        uri = '/v1.1/customer/%s' % customer_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET, Customer.api_server)

    def get_customer_children_by_customer_id(self, customer_id):
        uri = '/v1.1/customerChildren/%s' % customer_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET, Customer.api_server)

    def get_customer_children(self):
        uri = '/v1.1/customerChildren/'
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_GET, Customer.api_server)

    def create_customer(self, name, parent_id, type, partnership):
        uri = '/v1.1/createCustomer'
        request_body = '{\
                        "name": "%s",\
                        "parentId": %d,\
                        "type": %d,\
                        "partnership": %d\
                    }' % (name, parent_id, type, partnership)

        return HttpUtil.request_api(uri, request_body, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_POST,Customer.api_server)

    def delete_customer(self, customer_id):
        uri = '/v1.1/deleteCustomer/%s' % customer_id
        return HttpUtil.request_api(uri, None, self.access_key_id, self.access_key_secret, HttpUtil.HTTP_DELETE, Customer.api_server)