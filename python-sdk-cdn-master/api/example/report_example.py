# -*- coding: UTF-8 -*-
from api.report import Report

report = Report('your access key id (Under Settings -> Access Keys)', 'your key secret (Under Settings -> Access Keys)')

# report.get_bandwidth('["report.conversant.com.cn"]', '2018-11-27T04:00:00Z', '2018-11-27T04:10:00Z',  'true', 'minute')
# report.get_origin_bandwidth('["report.conversant.com.cn"]', '2018-11-27T04:00:00Z', '2018-11-27T04:10:00Z',  'true', 'minute')

# report.get_request_hit_number('["report.conversant.com.cn"]', '2018-11-27T05:45:00Z', '2018-11-27T05:50:00Z',  'false', 'minute')
# report.get_origin_request_number('["report.conversant.com.cn"]', '2018-11-27T05:45:00Z', '2018-11-27T05:50:00Z',  'true', 'minute')
# report.get_request_hit_rate('["report.conversant.com.cn"]', '2018-11-27T05:45:00Z', '2018-11-27T05:50:00Z',  'true', 'minute')
# report.get_request_number('["report.conversant.com.cn"]', '2018-11-27T05:45:00Z', '2018-11-27T05:50:00Z',  'false', 'minute')

# report.get_http_codes('["report.conversant.com.cn"]', '2018-11-27T06:05:00Z', '2018-11-27T06:10:00Z')

# report.get_hit_volume('["report.conversant.com.cn"]', '2018-11-27T04:20:00Z', '2018-11-27T04:30:00Z',  'true', 'minute')
report.get_volume('["report.conversant.com.cn"]', '2018-11-27T04:20:00Z', '2018-11-27T04:30:00Z',  'false', 'minute')
# report.get_volume_v11('["report.conversant.com.cn"]', '2018-11-27T00:00:00Z', '2018-11-28T00:00:00Z',  'false', 'day')

# report.get_customer_volume(30017, '2018-11-25T00:00:00Z', '2018-11-26T04:30:00Z')
# report.get_customer_bandwidth(30017, '2018-11-25T00:00:00Z', '2018-11-26T04:30:00Z')

# report.list_logs('["report.conversant.com.cn"]', '2018-11-26T04:00:00Z', '2018-11-26T04:30:00Z')
# report.list_logs_v11('["report.conversant.com.cn"]', '2018-11-26T04:00:00Z', '2018-11-26T04:30:00Z')
