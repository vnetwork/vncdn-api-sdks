# -*- coding: UTF-8 -*-
from api.domain import Domain

domain = Domain('your access key id (Under Settings -> Access Keys)', 'your key secret (Under Settings -> Access Keys)')
domain.list_domain(30017)
# domain.create_domain(30017, 'zjkwsa.com','1.2.3.4')
# domain.edit_domain(225623, '4.3.2.1', 'true', 'false')
# domain.update_domain_status(225623, 'true')
# domain.delete_domain(225623)
# domain.get_domain(225608)

# domain.list_lfd_domain(30017)
# domain.create_lfd_domain(30017, 'zjklfd.com','123')
# domain.edit_lfd_domain(225624, '456', 'true', 'false')
# domain.update_lfd_domain_status(225624, 'true')
# domain.get_lfd_domain(225624)
# domain.delete_lfd_domain(225624)
