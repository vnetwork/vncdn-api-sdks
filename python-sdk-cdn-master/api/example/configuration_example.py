# -*- coding: UTF-8 -*-
from api.configuration import Configuration

configuration=Configuration('your access key id (Under Settings -> Access Keys)', 'your key secret (Under Settings -> Access Keys)')

# configuration.list_purges(225446, "domain")
# configuration.create_purge(225446,"/resources/js/test2.js",1543306335,"domain")

# configuration.list_prefetches_page(225446, 1)
# configuration.create_prefetch(225446,'[\
#        "https://cdn.example1.com/resources/load116.js?v=1.2.0",\
#        "https://cdn.example1.com/resources/load115.js?v=1.2.0",\
#        "https://cdn.example1.com/resources/load114.js?v=1.2.0"\
#    ]')

# configuration.list_access_controls(225446)
# configuration.create_access_control(225446,'ac11','allow','regex','/2/*.js','192.168.1.0/24','SN,CN')
# configuration.edit_access_control(225446,257145,'ac11','allow','regex','/3/*.js','192.168.1.0/24','SN,CN')
# configuration.delete_access_control(225446,257145)

# configuration.list_cache_controls(225446)
# configuration.create_cache_control(225446,'zjk','regex','/3/*.js','cdn.example1-2.com',86400,False,True,True)
# configuration.edit_cache_control(225446,257146,'zjk','regex','/4/*.js','cdn.example1-2.com',86400,False,True,True)
# configuration.delete_cache_control(225446,257146)

configuration.list_redirections(225446)
# configuration.create_redirection(225446,'zjk','regex','/4/*.js','http://www.example.com/404.html',301)
# configuration.edit_redirection(225446,257149,'zjkok','regex','/5/*.js','http://www.example.com/404.html',301)
# configuration.delete_redirection(225446,257149)